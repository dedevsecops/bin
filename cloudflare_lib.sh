#!/bin/bash

# Requires env vars:
#
# * cloudflare_token=<gitlab access token with api scope>

cloudflare_init () {
  if [[ -z $cloudflare_token ]]; then
    echo "${0} ERROR: cloudflare_token env var must be set" >&2
  fi
  #export git_lib_var_dir="${HOME}/var"
  #mkdir -p "${git_lib_var_dir}"
}

cloudflare_pagination_wrapper () {
  api_path=$1
  optional_query=$2
  page=1
  per_page=20
  order='type'
  direction='asc'
  rate_limit=100
  while true; do
    if [[ $page -gt $rate_limit ]]; then
      echo "ERROR: rate_limit is set to ${rate_limit}. If you actually have more pages, adjust this script." >&2
      break
    fi
    query="page=${page}&per_page=${per_page}&order=${order}&direction=${direction}${optional_query}"
    output=$(curl -s -H "Authorization: Bearer ${cloudflare_token}" -H "Content-Type:application/json" "https://api.cloudflare.com/client/v4${api_path}?${query}" | jq .)
    result=$(echo $output | jq .result)
    case $result in
      '[]') break;;
      '{}') break;;
      *) echo "$output";;
    esac
    ((page++))
  done
}

cloudflare_zones () {
  cloudflare_pagination_wrapper "/zones" | \
  jq -r '.result[].id'
}

cloudflare_dns_records () {
  for zone_id in $(cloudflare_zones); do
    cloudflare_pagination_wrapper "/zones/${zone_id}/dns_records" | jq .
  done
}

#gitlab_groups_members () {
#  gitlab_pagination_wrapper "/v4/groups/${1}/members" | \
#  jq -cr '.[] | {"username":.username,"name":.name,"access_level":.access_level}'
#}
#
#gitlab_projects () {
#  gitlab_pagination_wrapper '/v4/projects' '&membership=true' | \
#  jq -r '.[] | select(.archived==false) | .ssh_url_to_repo'
#}
#
#gitlab_projects_archived () {
#  gitlab_pagination_wrapper '/v4/projects' '&membership=true' | \
#  jq -r '.[] | select(.archived==true) | .ssh_url_to_repo'
#}
#
#gitlab_projects_list () {
#  gitlab_init
#  gitlab_projects | sort > "${git_lib_var_dir}/gitlab_projects.txt"
#  gitlab_projects_archived | sort > "${git_lib_var_dir}/gitlab_projects_archived.txt"
#}
