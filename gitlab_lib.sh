#!/bin/bash

# Requires env vars:
#
# * gitlab_token=<gitlab access token with api scope>

gitlab_init () {
  if [[ -z $gitlab_token ]]; then
    echo "${0} ERROR: gitlab_token env var must be set" >&2
  fi
  export git_lib_var_dir="${HOME}/var"
  mkdir -p "${git_lib_var_dir}"
}

gitlab_pagination_wrapper () {
  api_path=$1
  optional_query=$2
  page=1
  rate_limit=100
  while true; do
    if [[ $page -gt $rate_limit ]]; then
      echo "ERROR: rate_limit is set to ${rate_limit}. If you actually have more pages, adjust this script." >&2
      break
    fi
    query="page=${page}&per_page=100${optional_query}"
    output=$(curl -s -H "Private-Token: ${gitlab_token}" "https://gitlab.com/api${api_path}?${query}" | jq .)
    case $output in
      '[]') break;;
      '{}') break;;
      *) echo "$output";;
    esac
    ((page++))
  done
}

gitlab_groups () {
  gitlab_pagination_wrapper '/v4/groups' | \
  jq -cr '.[] | {"id":.id,"web_url":.web_url}'
}

gitlab_groups_members () {
  gitlab_pagination_wrapper "/v4/groups/${1}/members" | \
  jq -cr '.[] | {"username":.username,"name":.name,"access_level":.access_level}'
}

gitlab_projects () {
  gitlab_pagination_wrapper '/v4/projects' '&membership=true' | \
  jq -r '.[] | select(.archived==false) | .ssh_url_to_repo'
}

gitlab_projects_archived () {
  gitlab_pagination_wrapper '/v4/projects' '&membership=true' | \
  jq -r '.[] | select(.archived==true) | .ssh_url_to_repo'
}

gitlab_projects_list () {
  gitlab_init
  gitlab_projects | sort > "${git_lib_var_dir}/gitlab_projects.txt"
  gitlab_projects_archived | sort > "${git_lib_var_dir}/gitlab_projects_archived.txt"
}
