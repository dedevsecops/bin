#!/bin/bash

if [[ ! -d "${PWD}/.git" ]]; then
  echo "ERROR: This doesn't appear to be a git repository."
  exit 1
fi

export temp_file=$(mktemp)

# Based on 'pre-commit sample-config'
cat <<EOF > "${temp_file}"
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
- repo: https://github.com/pre-commit/pre-commit-hooks
  rev: v4.2.0
  hooks:
  - id: trailing-whitespace
  - id: end-of-file-fixer
  - id: check-yaml
    args: [--allow-multiple-documents]
  - id: check-added-large-files
- repo: https://github.com/zricethezav/gitleaks
  rev: v8.8.2
  hooks:
  - id: gitleaks-docker
    name: Detect hardcoded secrets
    description: Detect hardcoded secrets using Gitleaks
    entry: zricethezav/gitleaks protect --verbose --redact --staged
    language: docker_image
EOF

if [[ -f .pre-commit-config.yaml ]]; then
  if ! diff -s .pre-commit-config.yaml "${temp_file}" &> /dev/null; then
    diff -y .pre-commit-config.yaml "${temp_file}"
    read -p "Make these changes [y/n]? "
    if [[ "$REPLY" == 'y' ]]; then
      mv -v "${temp_file}" .pre-commit-config.yaml
    else
      echo "INFO: No changes made."
      exit
    fi
  else
    echo "INFO: .pre-commit-config.yaml already exists and is the same as proposed version."
    exit
    rm "${temp_file}"
  fi
else
  read -p "No .pre-commit-config.yaml file found. Create a new one [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    mv -v "${temp_file}" .pre-commit-config.yaml
  else
    echo "INFO: No changes made."
    exit
  fi
fi

pre-commit install

cat <<EOF

Trying running this until everything passes:

pre-commit run --all-files

EOF
