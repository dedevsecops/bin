#!/bin/bash

# This reads your AWS config, and for every profile it finds, it searches all
# regions for AWS EKS clusters. For each EKS cluster it finds, it outputs the
# appropriate "aws eks update-kubeconfig" command. You can then copy/paste
# these commands for the clusters you want to access.

#TODO:
# * This does not accomodate clusters that require a `--role` to use.
# * If you have multiple profiles for the same AWS account, it may output
# multiple `update-kubeconfig` commands, but if you run them all, only the last
# `update-kubeconfig` command will be effective. When I last checked,
# `update-kubeconfig` wouldn't create new kubeconfig contexts per profile, it
# just creates one per cluster.

while read profile; do
  #echo "# AWS profile: ${profile}"
  export AWS_DEFAULT_REGION='us-east-1'
  export AWS_PROFILE="${profile}"
  regions_json=$(aws ec2 describe-regions 2> /dev/null)
  regions=($(echo "$regions_json" | jq -r .Regions[].RegionName))
  for region in "${regions[@]}"; do
    #echo "## AWS region: ${region}"
    export AWS_REGION="${region}"
    clusters_json=$(aws eks list-clusters --region $AWS_REGION 2>/dev/null)
    clusters=($(echo "$clusters_json" | jq -r .clusters[]))
    for cluster in "${clusters[@]}"; do
      #echo "### AWS EKS cluster: ${cluster}"
      name="$(aws eks describe-cluster --region $AWS_REGION --name $cluster | jq -r '.cluster.name')"
      version="$(aws eks describe-cluster --region $AWS_REGION --name $cluster | jq -r '.cluster.version')"
      if [[ -n "$name" ]]; then
        echo "aws eks update-kubeconfig --alias \"${name}\" --name \"${name}\" --profile \"${profile}\" --region \"${AWS_REGION}\""
      fi
    done
  done
done < <(sed -nE "s/^\[profile ['\"]*([^'\"]+)['\"]*\]/\1/p" "${HOME}/.aws/config")
