#!/bin/bash

# $0 chart_url chart_name old_chart_version new_chart_version values_files

set -x


temp_helm_repo="deleteme-$(date +%s)"
chart_url="$1"
helm repo add "$temp_helm_repo" "$chart_url"

# Create
chart_name="$2"
old_chart_version="$3"
new_chart_version="$4"

echo "$@"

exit

temp_values_file="$(mktemp)"
helm show values "${temp_helm_repo}/${chart_name}" --version "$chart_version" > "$temp_values_file"


values_files="$@"
#dyff between "$temp_values_file" <(yq m -x "$temp_values_file" "$values_files[@]")
dyff between "$temp_values_file" <(yq ea '. as $item ireduce ({}; . * $item )' "$temp_values_file" "${values_files[@]}")

#/tmp/dyff between <(yq m -x prometheus-operator.yaml.gotmpl alert-manager.yaml secrets.prometheus-operator.yaml) <(yq m -x common/kube-prometheus-stack.yaml.defaults common/kube-prometheus-stack.yaml common/kube-prometheus-stack.yaml.gotmpl common/secrets.kube-prometheus-stack.yaml)



helm repo delete "${temp_helm_repo}"
rm -v "$values_file"
