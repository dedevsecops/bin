# bin

Clone this repo and add it to your PATH to use these scripts.

For example:

```
mkdir -p "${HOME}/gitlab.com/dedevsecops"
cd "${HOME}/gitlab.com/dedevsecops"
git clone git@gitlab.com:dedevsecops/bin.git
echo 'export PATH="${PATH}:${HOME}/gitlab.com/dedevsecops/bin"' >> "${HOME}/.profile"
```
