#!/bin/bash

if [[ $# -ne 1 ]]; then
  cat <<EOF
Usage:

audit_gitlab.sh <allowed users file>

where, the allowed users file is a newline separated list of allowed gitlab usernames.
EOF
exit 1
fi

export allowed_users_file="$1"
export git_lib_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${git_lib_dir}/gitlab_lib.sh"

for gitlab_group in $(gitlab_groups); do
  gitlab_group_id=$(echo "$gitlab_group" | jq -r .id)
  gitlab_group_web_url=$(echo "$gitlab_group" | jq -r .web_url)
  output="${gitlab_group_web_url}/-/group_members?sort=access_level_desc\n"

  # Reset found_user before entering loop again.
  found_user=false

  while read gitlab_member; do
    gitlab_member_username=$(echo "$gitlab_member" | jq -r .username)
    gitlab_member_name=$(echo "$gitlab_member" | jq -r .name)
    gitlab_member_access_level=$(echo "$gitlab_member" | jq -r .access_level)
    if ! grep -qE "^${gitlab_member_username}$" "${allowed_users_file}"; then
      output+="* ${gitlab_member_username} (${gitlab_member_access_level}) aka \"${gitlab_member_name}\" is not authorized\n"
      found_user=true
    fi
  done < <(gitlab_groups_members "$gitlab_group_id")
  if $found_user; then
    echo -e "$output"
  fi
done
