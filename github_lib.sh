#!/bin/bash

# Requires env vars:
#
# * github_token=<github personal access token with repo scope>

github_init () {
  if [[ -z $github_token ]]; then
    echo "${0} ERROR: github_token env var must be set" >&2
  fi
  export git_lib_var_dir="${HOME}/var"
  mkdir -p "${git_lib_var_dir}"
}

github_pagination_wrapper () {
  api_path=$1
  optional_query=$2
  page=1
  rate_limit=100
  while true; do
    if [[ $page -gt $rate_limit ]]; then
      echo "ERROR: rate_limit is set to ${rate_limit}. If you actually have more pages, adjust this script." >&2
      break
    fi
    query="page=${page}&per_page=100${optional_query}"
    output=$(curl -s -H "Authorization: token ${github_token}" "https://api.github.com${api_path}?${query}" | jq .)
    case $output in
      '[]') break;;
      '{}') break;;
      *) echo "$output";;
    esac
    ((page++))
  done
}

github_repos () {
  github_pagination_wrapper '/user/repos' | \
  jq -r '.[] | select(.archived==false) | .ssh_url'
}

github_repos_archived () {
  github_pagination_wrapper '/user/repos' | \
  jq -r '.[] | select(.archived==true) | .ssh_url'
}

github_repos_list () {
  github_init
  github_repos | sort > "${git_lib_var_dir}/github_repos.txt"
  github_repos_archived | sort > "${git_lib_var_dir}/github_repos_archived.txt"
}

github_consensys_members () {
  github_pagination_wrapper '/orgs/ConsenSys/members' | \
  jq -r '.[].login'
}
