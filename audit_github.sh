#!/bin/bash

if [[ $# -ne 1 ]]; then
  cat << EOF
Usage:

audit_github.sh <allowed users file>

where, the allowed users file is a newline separated list of allowed github usernames.
EOF
  exit 1
fi

export allowed_users_file="$1"
export git_lib_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${git_lib_dir}/github_lib.sh"

output=''
for member in $(github_consensys_members); do
  if ! grep -qE "^${member}$" "${allowed_users_file}"; then
    output+="${member}\n"
  fi
done
echo -e "Unauthorized Users:\n$output"

exit

for github_repo in $(github_repos); do
  #github_repo_id=$(echo "$github_repo" | jq -r .id)
  #github_repo_web_url=$(echo "$github_repo" | jq -r .web_url)
  output="${github_repo_web_url}/-/group_members?sort=access_level_desc\n"

  # Reset found_user before entering loop again.
  found_user=false

  while read github_member; do
    github_member_username=$(echo "$github_member" | jq -r .username)
    github_member_name=$(echo "$github_member" | jq -r .name)
    github_member_access_level=$(echo "$github_member" | jq -r .access_level)
    if ! grep -qE "^${github_member_username}$" "${allowed_users_file}"; then
      output+="* ${github_member_username} (${github_member_access_level}) aka \"${github_member_name}\" is not authorized\n"
      found_user=true
    fi
  done < <(github_repos_members "$github_repo_id")
  if $found_user; then
    echo -e "$output"
  fi
done
