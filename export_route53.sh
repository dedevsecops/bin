#!/bin/bash

if [[ $# -eq 0 ]]; then
  echo "Usage: $0 <AWS profile>"
  exit 1
fi

export AWS_PROFILE="$1"
zone_json="$(aws route53 list-hosted-zones | jq -r .)"
for zone_name in $(echo "$zone_json" | jq -r '.HostedZones[].Name'); do
  zone_id=$(echo "$zone_json" | jq -r ".HostedZones[] | select(.Name==\"${zone_name}\") | .Id")
  echo "Exporting ${zone_name} (${zone_id})..."

  # No dot between zone_name and json in the filename, because zone_name has a trailing dot.
  aws route53 list-resource-record-sets --hosted-zone-id "${zone_id}" > "${zone_name}json"

done
