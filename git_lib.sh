#!/bin/bash

git_lib_init () {
  export git_lib_var_dir="${HOME}/var"
  mkdir -p "${git_lib_var_dir}"
  export git_service_dirs=("${HOME}/github.com" "${HOME}/gitlab.com")
}

git_repos_list () {
  git_lib_init
  github_repos_list
  gitlab_projects_list
  cat "${git_lib_var_dir}/github_repos.txt" "${git_lib_var_dir}/gitlab_projects.txt" | sort > "${git_lib_var_dir}/git_repos_new.txt"
  cat "${git_lib_var_dir}/github_repos_archived.txt" "${git_lib_var_dir}/gitlab_projects_archived.txt" | sort > "${git_lib_var_dir}/git_repos_archived_new.txt"
  echo "===== git_repos.txt"
  diff "${git_lib_var_dir}/git_repos.txt" "${git_lib_var_dir}/git_repos_new.txt"
  echo "===== git_repos_archived.txt"
  diff "${git_lib_var_dir}/git_repos_archived.txt" "${git_lib_var_dir}/git_repos_archived_new.txt"
  read -p "Make these changes [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    mv "${git_lib_var_dir}/git_repos_new.txt" "${git_lib_var_dir}/git_repos.txt"
    mv "${git_lib_var_dir}/git_repos_archived_new.txt" "${git_lib_var_dir}/git_repos_archived.txt"
  fi
}

git_rebuild_excluded() {
  git_lib_init

  tmp_file=$(mktemp)

  # Adding all repos to excluded by default.
  cat "${git_lib_var_dir}/github_repos.txt" "${git_lib_var_dir}/gitlab_projects.txt" > "${tmp_file}"

  # Removing case insensitive matches to keywords I care about from the excluded list.
  sed -i -E '/(codefi|cofi|evanstucker|dedevsecops|infra|cpag|dauriel|orchestrate|consensys-defi|alloy|defi-score|filecoin|metaswap|meridio|balanc3|ops|ps-devops|pukara|unleash|core-stack|palm|staking)/Id' "${tmp_file}"

  # Adding all audits back to excluded repos.
  grep -h audit "${git_lib_var_dir}/github_repos.txt" "${git_lib_var_dir}/gitlab_projects.txt" >> "${tmp_file}"

  # Sorting for maximum anal retentiveness
  sort "${tmp_file}" > "${tmp_file}.sorted"
  mv "${tmp_file}.sorted" "${tmp_file}"

  diff "${git_lib_var_dir}/excluded_repos.txt" "${tmp_file}"
  read -p "Make these changes [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    mv "${tmp_file}" "${git_lib_var_dir}/excluded_repos.txt"
  fi
}

git_delete_repos () {
  git_lib_init
  git_find_git_dirs
  export active_repos=(
    $(sed "s#git@#${HOME}/#;s#:#/#;s#\.git\$##;" "${git_lib_var_dir}/git_repos.txt")
  )
  export dirs_to_delete=()
  for git_dir in "${git_dirs[@]}"; do
    if ! (printf '%s\n' "${active_repos[@]}" | grep -q "^${git_dir}\$"); then
      dirs_to_delete+=("$git_dir")
    fi
  done
  echo 'Directories to delete (with git status):'
  for d in "${dirs_to_delete[@]}"; do
    echo "$d"
    cd "$d"
    git status --short
    cd - &> /dev/null
  done
  read -p "Delete these directories [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    rm -frv "${dirs_to_delete[@]}"
  fi
}

git_get_my_repos () {
  git_lib_init
  while read -r git_url; do
    vendor=$(echo "$git_url" | cut -d '@' -f 2 | cut -d ':' -f 1)
    repo=$(echo "$git_url" | cut -d ':' -f 2- | sed 's/\.git$//')
    git_dir="${HOME}/${vendor}/${repo}"
    echo "===== ${git_dir}"

    if [[ -d "$git_dir" ]]; then
      cd "$git_dir"
      git_fresh
    else
      mkdir -p "$git_dir"
      cd "$git_dir"
      git clone -q "$git_url" "$git_dir"
    fi
  done < <(grep -v -f "${git_lib_var_dir}/excluded_repos.txt" "${git_lib_var_dir}/git_repos.txt")
}

# This is not needed if you run git_rebuild_excluded.
git_remove_archived_from_excluded () {
  git_lib_init

  # Remove archived repos from excluded repos
  tmp_file=$(mktemp)
  grep -v -f "${git_lib_var_dir}/git_repos_archived.txt" "${git_lib_var_dir}/excluded_repos.txt" > "${tmp_file}"
  diff "${git_lib_var_dir}/excluded_repos.txt" "${tmp_file}"
  read -p "Make these changes [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    mv "${tmp_file}" "${git_lib_var_dir}/excluded_repos.txt"
  fi

  # Only include repos that we have access to in excluded repos (don't need to exclude it if we can't access it anyway).
  tmp_file=$(mktemp)
  grep -f "${git_lib_var_dir}/git_repos.txt" "${git_lib_var_dir}/excluded_repos.txt" > "${tmp_file}"
  diff "${git_lib_var_dir}/excluded_repos.txt" "${tmp_file}"
  read -p "Make these changes [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    mv "${tmp_file}" "${git_lib_var_dir}/excluded_repos.txt"
  fi

}

git_delete_archived_and_excluded_repos () {
  git_lib_init
  export dirs_to_delete="$(sed "s#git@#${HOME}/#;s#:#/#;s#\.git\$##;" "${git_lib_var_dir}/git_repos_archived.txt" "${git_lib_var_dir}/excluded_repos.txt" )"
  echo "Directories to be deleted:"
  while read git_dir; do
    if [[ -d "${git_dir}" ]]; then
      echo "${git_dir}"
    fi
  done < <(echo "$dirs_to_delete")
  read -p "Delete these directories [y/n]? "
  if [[ "$REPLY" == 'y' ]]; then
    while read git_dir; do
      if [[ -d "${git_dir}" ]]; then
        #rm -rvf "${git_dir}"
        ls -lad "${git_dir}"
      fi
    done < <(echo "$dirs_to_delete")
  fi
}

git_find_git_dirs () {
  git_lib_init

  # Excluding paths with .terraform because some Terraform modules have .git dirs in them...
  export git_dirs=(
    $(find "${git_service_dirs[@]}" -type d -name .git -exec sh -c 'readlink -m $(dirname "$0")' {} ';' | grep -vF '/.terraform/')
  )

}

git_get_everything () {
  git_lib_init
  git_find_git_dirs
  for git_dir in "${git_dirs[@]}"; do
    echo "===== ${git_dir}"
    cd "$git_dir"
    git_fresh
  done
}

git_fresh () {
  # This script fetches everything, then finds the default git branch of a repo
  # and checks it out. It will not overwrite pending changes. It just freshens
  # everything up and sets you back on the default branch, so you're in the right
  # place next time you work in the repo.

  git fetch \
    --all \
    --prune \
    --prune-tags \
    --quiet \
    --recurse-submodules=yes \
    --tags

  # You must run this before checking the for the default branch, or you risk
  # using the wrong default branch.
  git remote set-head origin --auto 1>/dev/null

  # https://stackoverflow.com/questions/28666357/git-how-to-get-default-branch
  default_branch="$(git symbolic-ref refs/remotes/origin/HEAD 2> /dev/null | sed 's@^refs/remotes/origin/@@')"

  if [[ -z $default_branch ]]; then
    git checkout --quiet
  else
    git checkout --quiet "${default_branch}"
  fi

  git pull --quiet

  git status --short

  # This section outputs a list of commands that you can copy and paste to delete merged branches.
  if [[ "$1" == 'delete_merged' ]]; then

    merged_branches=$(git branch -a --merged "${default_branch}" | sed -E "/\b(HEAD|master|${default_branch})\b/d")

#    if [[ ! -z $merged_branches ]]; then
      cat <<EOF

Merged branches:

${merged_branches}

You can delete them with these commands:

git branch -r --merged "${default_branch}" | sed -E "s#origin/##;/\b(HEAD|master|${default_branch})\b/d" | xargs -n 1 git push origin --delete
git branch --merged "${default_branch}" | sed -E "/\b(HEAD|master|${default_branch})\b/d" | xargs -n 1 git branch -d

EOF
#    fi
  fi
}

########################################## MAIN ###############################

export git_lib_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${git_lib_dir}/github_lib.sh"
source "${git_lib_dir}/gitlab_lib.sh"
source "${git_lib_dir}/bitbucket_lib.sh"

###### Attempting to refactor to GraphQL - I don't know what the hell I'm doing.
#
#q_currentUser='{"query":"query{currentUser{name}}"}'
#
#q_group='{"query":"query{group(fullPath:\"gitlab-org\"){id name projects{nodes{name}}}}"}'
#
##curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data '{"query": "query {currentUser {name}}"}' | jq .
#
#echo "$q_currentUser" | jq -cr .
#
#curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data "$q_currentUser" | jq .
#
#echo "$q_group" | jq -cr .
#
#curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data "$q_group" | jq .
