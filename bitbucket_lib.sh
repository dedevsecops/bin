#!/bin/bash

bitbucket_repositories () {
  curl -s --user "${bb_username}:${bb_password}" "https://api.bitbucket.org/2.0/user/permissions/repositories" | \
  jq -r '.values[] | "git@bitbucket.org:\(.repository.full_name).git"'
}
